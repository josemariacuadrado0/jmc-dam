<?php 

/*
 * (c) Jose Cuadrado
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace JmcdamComposer;

class Jmcdam
{
    public function say($toSay = "Nothing given")
    {
        return $toSay;
    }
}